from flask import Flask, flash, redirect, render_template, request, session, abort
from sales_regression_model import Sales_regression_model
from rental_regression_model import Rental_regression_model
from sklearn.preprocessing import LabelEncoder
from sales_regression_model_views import Sales_regression_model_views
import json


app = Flask(__name__)

@app.route("/")
def index():
    return render_template('index.html')



@app.route("/sales_prediction", methods = ['POST','GET'])
def sales_prediction():
    temp_prediction_model_views= Sales_regression_model_views()
    temp_prediction_model = Sales_regression_model()
    if request.form['sales_button']=="Estimate":

        print("sales model being called...")
        house_type= request.form['House Type']
        house_area_name = request.form['Area']
        if request.form['Town']:
            house_town_name = request.form['Town']
        else:
            house_town_name= None

        if request.form['Number of Bedrooms']:
            no_bedrooms = request.form['Number of Bedrooms']
        else:
            no_bedrooms = None

        if request.form['Number of Washrooms']:
            no_washrooms = request.form['Number of Washrooms']
        else:
            no_washrooms = None

        temp_vals=[no_bedrooms,no_washrooms,house_area_name,house_town_name,house_type]

        if None in temp_vals:
            return render_template('index.html', evaluation_tag="Error: Missing values found..")
        else:
            temp_data = temp_prediction_model.prediction(json.dumps(temp_vals))
            temp_vals_views = [temp_data,no_bedrooms,no_washrooms,house_area_name,house_town_name,house_type]
            views = temp_prediction_model_views.prediction(json.dumps(temp_vals_views))

            return render_template('index.html', evaluation_tag="Sales", data = temp_data, views=views)


    elif request.form['sales_button']=="Train Sales Model":
        print("sales model being trained...")

        temp_prediction_model_views.model_train()
        return render_template('index.html')






@app.route("/rental_prediction",methods = ['POST','GET'])
def rental_prediction():
    temp_prediction_model = Rental_regression_model()
    if request.form['rental_button']=="Estimate":

        print("rental model being called...")
        house_type= request.form['House Type']
        house_area_name = request.form['Area']

        if request.form['Number of Bedrooms']:
            no_bedrooms = request.form['Number of Bedrooms']
        else:
            no_bedrooms = None

        if request.form['Number of Washrooms']:
            no_washrooms = request.form['Number of Washrooms']
        else:
            no_washrooms = None

        temp_vals=[no_bedrooms,no_washrooms,house_area_name,house_type]

        if None in temp_vals:
            return render_template('index.html', evaluation_tag="Error: Missing values found..")
        else:
            temp_data = temp_prediction_model.prediction(json.dumps(temp_vals))
            return render_template('index.html', evaluation_tag="Rental", data = temp_data)

    elif request.form['rental_button']=="Train Rental Model":
        print("rental model being trained...")
        temp_prediction_model.model_train()
        return render_template('index.html')



    new_input = [[2,1,11,0]]

    temp =  temp_prediction_model.prediction(json.dumps(new_input))
    temp = json.loads(temp)

    return temp


@app.route("/hello")
def hello():
    return render_template('index.html')

if __name__ == "__main__":
    app.run(debug= True)
