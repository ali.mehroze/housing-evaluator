#!/usr/bin/env python
# coding: utf-8

# In[22]:


import pandas as pd

df = pd.read_csv("housing_rental.csv")

df.dtypes


# In[23]:


#remove missing values
print len(df)
df = df.dropna()
print len(df)
print df.total_views
print df.head()


# In[24]:


#typecasting
df.no_bedrooms = pd.to_numeric(df.no_bedrooms, errors='coerce')
df.no_washrooms = pd.to_numeric(df.no_washrooms, errors='coerce')

df['house_rent'] = df['house_rent'].astype('str')
# df.house_price = df.house_price.replace(",","")
df['total_views'] = df['total_views'].astype('str')

for i, rows in df.iterrows():
#     rows.house_price = rows.house_price.replace(",","")
    df.at[i,'house_rent'] = df.at[i,'house_rent'].replace(",","")
    df.at[i,'total_views'] = df.at[i,'total_views'].replace(",","")
# print df.house_price

df.house_rent = pd.to_numeric(df.house_rent, errors='coerce')
df.total_views = pd.to_numeric(df.total_views, errors='coerce')
df.total_views = df.total_views.astype(int)
df.no_washrooms = df.no_washrooms.astype(int)
df = df.dropna()
df.dtypes
df


# In[25]:


df["house_town_name"] = df.house_location.str.split(",").str[-2] 
df["house_area_name"] = df.house_location.str.split(",").str[-1] 
df


# In[36]:


df.dtypes


# In[26]:


df1 = df
for i,rows in df1.iterrows():
    if df1.at[i,'term']=='week':
        df1.at[i,'house_rent']=df1.at[i,'house_rent']*4
    


# In[34]:


df1 = df1.drop(columns='term')


# In[37]:


df1.dtypes


# In[41]:


df1.to_csv(r'housing_rental_cleaned.csv',index=None, header=True,encoding='utf-8-sig')


# In[39]:


df1

