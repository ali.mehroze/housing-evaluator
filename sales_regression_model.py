#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import r2_score
import json
import pickle
from sklearn.tree import DecisionTreeRegressor
from sklearn.metrics import make_scorer
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split
from sklearn.model_selection import ShuffleSplit

class Sales_regression_model():

    def model_train(self):

        df = pd.read_csv('housing_sale_cleaned.csv')


        df.head()
        df.dtypes
        df1 = df

        #removing outliers
        df1 = df1[df1.house_price<=1000000]
        df1 = df1[df1.no_bedrooms<=7]
        df1 = df1[df1.no_washrooms<=7]


        for i, rows in df1.iterrows():
            df1.at[i,'house_area_name'] = df1.at[i,'house_area_name'].strip()
            df1.at[i,'house_town_name'] = df1.at[i,'house_town_name'].strip()
            df1.at[i,'house_type'] = df1.at[i,'house_type'].strip()

        df1["house_area_name"] = df1["house_area_name"].str.replace("Dublin 6w","Dublin 6")

        df1_encoded = df1.copy()
        label_make1 = LabelEncoder()
        label_make2 = LabelEncoder()
        label_make3 = LabelEncoder()
        df1_encoded['area_code'] = label_make1.fit_transform(df1['house_area_name'])
        df1_encoded['town_code'] = label_make2.fit_transform(df1['house_town_name'])
        df1_encoded['house_type_code'] = label_make3.fit_transform(df1['house_type'])
        print(df1_encoded[['house_area_name','area_code','house_town_name','town_code','house_type','house_type_code']].head(10))

        # In[ ]:


        # print(df1_encoded['house_area_name'].value_counts())


        df1_encoded_test = df1_encoded.drop(columns= ['total_views','house_location','date_renewed','house_town_name','house_area_name','house_type'])


        # sns.pairplot(df1_encoded_test,height=3)
        # plt.tight_layout()



        df1_encoded_test.shape
        corrmat = df1_encoded_test.corr()
        k = 7 #number of variables for heatmap
        cols = corrmat.nlargest(k, 'house_price')['house_price'].index
        cm = np.corrcoef(df1_encoded[cols].values.T)
        # sns.set(font_scale=1.25)
        # hm = sns.heatmap(cm, cbar=True, annot=True, square=True, fmt='.2f', annot_kws={'size': 10}, yticklabels=cols.values, xticklabels=cols.values)
        # plt.show()


        most_corr = pd.DataFrame(cols)
        most_corr.columns = ['Most Correlated Features']
        most_corr


        def performance_metric(y_true, y_predict):
            score = r2_score(y_true, y_predict)

            # Return the score
            return score


        # In[ ]:


        prices = df1_encoded_test['house_price']
        features = df1_encoded_test.drop('house_price',axis = 1)


        # In[ ]:




        # Training and testing subsets
        X_train, X_test, y_train, y_test = train_test_split(features, prices, test_size=0.2, random_state = 42)

        # Success
        print("Training and testing split was successful.")


        # In[ ]:




        def fit_model(X, y):

            cv_sets = ShuffleSplit(n_splits = 10, test_size = 0.20, random_state = 0)

            regressor = DecisionTreeRegressor()

            params = {'max_depth':[1,2,3,4,5,6,7,8,9,10]}

            scoring_fnc = make_scorer(performance_metric)

            grid = GridSearchCV(estimator=regressor, param_grid=params, scoring=scoring_fnc, cv=cv_sets)

            grid = grid.fit(X, y)

            return grid.best_estimator_


        # In[ ]:


        reg = fit_model(X_train, y_train)

        print("max_depth is {} for the best model.".format(reg.get_params()['max_depth']))


        #R Square value:
        reg.score(X_train,y_train)

        pickle.dump(reg, open("sales_model_dump.pkl", 'wb'))

        print("Model Trained and Saved")



    def prediction(self, new_input_obj):
        new_input = json.loads(new_input_obj)
        df1 = pd.read_csv('housing_sale_cleaned.csv')
        #removing outliers
        df1 = df1[df1.house_price<=1000000]
        df1 = df1[df1.no_bedrooms<=7]
        df1 = df1[df1.no_washrooms<=7]


        for i, rows in df1.iterrows():
            df1.at[i,'house_area_name'] = df1.at[i,'house_area_name'].strip()
            df1.at[i,'house_town_name'] = df1.at[i,'house_town_name'].strip()
            df1.at[i,'house_type'] = df1.at[i,'house_type'].strip()

        df1["house_area_name"] = df1["house_area_name"].str.replace("Dublin 6w","Dublin 6")

        df1_encoded = df1.copy()

        label_make1 = LabelEncoder()
        label_make2 = LabelEncoder()
        label_make3 = LabelEncoder()

        df1_encoded['area_code'] = label_make1.fit_transform(df1['house_area_name'])
        df1_encoded['town_code'] = label_make2.fit_transform(df1['house_town_name'])
        df1_encoded['house_type_code'] = label_make3.fit_transform(df1['house_type'])


        # print(label_make1.inverse_transform([18]))

        print(new_input)
        new_input[0] = int(new_input[0])
        new_input[1] = int(new_input[1])
        new_input[2]=label_make1.transform([new_input[2]])[0]
        new_input[3]=label_make2.transform([new_input[3]])[0]
        new_input[4]=label_make3.transform([new_input[4]])[0]
        print(new_input)
        reg = None
        with open("sales_model_dump.pkl", 'rb') as file:
            reg = pickle.load(file)


        temp = reg.predict([new_input])
        print(temp[0])







        return int(temp[0])
