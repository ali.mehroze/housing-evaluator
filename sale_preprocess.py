#!/usr/bin/env python
# coding: utf-8

# In[47]:


import pandas as pd

df = pd.read_csv("housing_sale.csv")

df.dtypes


# In[48]:


#remove missing values
print len(df)
df = df.dropna()
print len(df)
print df.total_views
print df.head()


# In[49]:


df.dtypes


# In[50]:


#typecasting
df.no_bedrooms = pd.to_numeric(df.no_bedrooms, errors='coerce')
df.no_washrooms = pd.to_numeric(df.no_washrooms, errors='coerce')

df['house_price'] = df['house_price'].astype('str')
# df.house_price = df.house_price.replace(",","")
df['total_views'] = df['total_views'].astype('str')

for i, rows in df.iterrows():
#     rows.house_price = rows.house_price.replace(",","")
    df.at[i,'house_price'] = df.at[i,'house_price'].replace(",","")
    df.at[i,'total_views'] = df.at[i,'total_views'].replace(",","")
# print df.house_price


df.house_price = pd.to_numeric(df.house_price, errors='coerce')
df.total_views = pd.to_numeric(df.total_views, errors='coerce')
df = df.dropna()
df.total_views = df.total_views.astype(int)
df.house_price = df.house_price.astype(int)
df.no_washrooms = df.no_washrooms.astype(int)
df.no_bedrooms = df.no_bedrooms.astype(int)
df.dtypes
df


# In[51]:


df["house_town_name"] = df.house_location.str.split(",").str[-2] 
df["house_area_name"] = df.house_location.str.split(",").str[-1] 
df


# In[53]:


df.dtypes


# In[55]:


df1= df
df1.date_renewed = pd.to_datetime(df1.date_renewed,infer_datetime_format=True)

    


# In[57]:


df1.date_renewed


# In[56]:


df1.dtypes


# In[58]:


df1.to_csv(r'housing_sale_cleaned.csv',index=None, header=True,encoding='utf-8-sig')


# In[59]:


df1

