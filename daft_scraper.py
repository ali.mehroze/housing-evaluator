# import libraries
import urllib2
from bs4 import BeautifulSoup
import re
import csv

header = ['house_price','house_location','no_bedrooms','no_washrooms','house_type','date_renewed','total_views']
data = []
data.append(header)
page_number = 0


with open('housing_sale.csv', 'a') as csv_file:
    writer = csv.writer(csv_file)
    writer.writerow(header)
    while page_number <=2000:

        page_url = 'https://www.daft.ie/dublin-city/property-for-sale/?offset=' + str(page_number)
        page = urllib2.urlopen(page_url)
        soup1 = BeautifulSoup(page, 'html.parser')

        for soup in soup1.findAll(attrs={'class':'PropertyCardContainer__container'}):
            row=[]
            house_price = soup.find('strong', attrs = {'class':'PropertyInformationCommonStyles__costAmountCopy'})
            try:
                house_price = house_price.text.encode('utf-8')
                house_price = house_price[3:]

                row.append(house_price)
            except:
                row.append(None)


            house_location = soup.find('a', attrs = {'class':'PropertyInformationCommonStyles__addressCopy--link'}).text

            row.append(house_location)

            try:
                no_bedrooms = soup.find(attrs = {'class':'QuickPropertyDetails__iconCopy'}).text

                row.append(no_bedrooms)


            except:
                row.append(None)


            try:
                no_washrooms = soup.find(attrs = {'class':'QuickPropertyDetails__iconCopy--WithBorder'}).text
                row.append(no_washrooms)
            except:
                row.append(None)

            try:
                house_type = soup.find(attrs = {'class':'QuickPropertyDetails__propertyType'}).text

                row.append(house_type)
            except:
                row.append(None)

            try:
                other_details = soup.find('a', attrs= {'class':'brandLink'})


                temp_page_url = urllib2.urlopen('https://www.daft.ie/dublin'+other_details.get('href'))
                temp_page = BeautifulSoup(temp_page_url,'html.parser')
                date_renewed = temp_page.findAll(attrs={'class':'PropertyStatistics__iconData'})[0].text
                total_views = temp_page.findAll(attrs={'class':'PropertyStatistics__iconData'})[1].text
                row.append(date_renewed)
                row.append(total_views)
            except:
                row.append(None)
                row.append(None)

            data.append(row)
            writer.writerow(row)

        page_number+=20
        print "Page number : "
        print page_number
