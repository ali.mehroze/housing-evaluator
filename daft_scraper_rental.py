 # import libraries
import urllib2
from bs4 import BeautifulSoup
import re
import csv

header = ['house_rent', 'term', 'house_location','house_type','no_bedrooms','no_washrooms','total_views']
data = []
data.append(header)
page_number = 0


with open('housing_rental.csv', 'a') as csv_file:
    writer = csv.writer(csv_file)
    writer.writerow(header)
    while page_number <=1400:
        print page_number
        print "\n"
        page_url = 'https://www.daft.ie/dublin-city/residential-property-for-rent/?s%5Badvanced%5D=1&s%5Bignored_agents%5D%5B0%5D=1551&searchSource=rental&offset=' + str(page_number)
        print page_url
        page = urllib2.urlopen(page_url)
        soup1 = BeautifulSoup(page, 'html.parser')

        for soup in soup1.findAll(attrs={'class':'box'}):
            row=[]
            other_details = soup.find('div', attrs= {'class':'search_result_title_box'})
            other_details = other_details.find('a')

            temp_page_url = urllib2.urlopen('https://www.daft.ie'+other_details.get('href'))

            try:
                temp_page = BeautifulSoup(temp_page_url,'html.parser')
                print 'https://www.daft.ie'+other_details.get('href')



            except Exception as e: print(e)


            temp = temp_page.find(attrs = {'class':'smi-object-info'})
            temp = temp.find(attrs = {'id':'smi-summary-items'})

            try:

                house_price = temp.find(attrs = {'id':'smi-price-string'}).text.encode('utf-8')
                house_price = house_price[3:]
                print house_price
                house_price = house_price.split(' ')

                row.append(house_price[0])
                row.append(house_price[2])


            except Exception as e:

                row.append(None)
                row.append(None)
                print(e)


            try:
                house_location = temp_page.find(attrs = {'id': 'address_box'})
                house_location = house_location.find('h1')
                house_location = list(house_location.stripped_strings)

                house_location = "\n\n".join(house_location).encode('utf-8').strip() if house_location else ""
                row.append(house_location)


            except Exception as e:
                print "house_location not found"
                print e
                row.append(None)

            try:

                features = temp_page.find(attrs = {'id': 'smi-summary-items'})

                counter = 0
                for feature in features.findAll(attrs = {'class': 'header_text'}):

                    try:
                        temp_feature = feature.text
                        if counter > 0:
                            temp_feature = temp_feature.split(" ")
                            row.append(temp_feature[0])
                        else:
                            row.append(temp_feature)
                        counter+=1


                    except Exception as e:
                        print e
                        row.append(None)
                        row.append(None)
                        row.append(None)




            except Exception as e:
                print e
                row.append(None)
                row.append(None)
                row.append(None)

            try:
                extras = temp_page.find(attrs= {'class':'description_extras'})


                temp_views = ""
                for temp in extras.findAll("h3"):
                    temp_views = temp

                total_views = temp_views.next_sibling
                total_views = total_views.strip()

                try:
                    row.append(total_views)
                except Exception as e:
                    print e
                    
            except Exception as e:
                print e
                row.append(None)
                row.append(None)

            try:
                data.append(row)
                writer.writerow(row)
                print row

            except Exception as e:
                print e

        page_number+=20
